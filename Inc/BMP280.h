/*
 * BMB280.h
 *
 *  Created on: 16.03.2018
 *      Author: robert
 */

#ifndef BMP280_H_
#define BMP280_H_

#include "main.h"
#include "stm32f4xx_hal.h"
#include "spi.h"
#include "usart.h"
#include "gpio.h"

typedef uint8_t bool_t;
typedef uint8_t status_t;

/* *************************************************************** */
/* ------------------ Preprocessor's definitions ----------------- */
/* *************************************************************** */

/* ---------------------- Registers adresses --------------------- */

#define BMP280_temp_xlsb_RegAddr   0xFC
#define BMP280_temp_lsb_RegAddr    0xFB
#define BMP280_temp_msb_RegAddr    0xFA
#define BMP280_press_xlsb_RegAddr  0xF9
#define BMP280_press_lsb_RegAddr   0xF8
#define BMP280_press_msb_RegAddr   0xF7
#define BMP280_config_RegAddr      0xF5
#define BMP280_ctrl_meas_RegAddr   0xF4
#define BMP280_status_RegAddr      0xF3
#define BMP280_reset_RegAddr       0xE0
#define BMP280_id_RegAddr          0xD0
#define BMP280_calib25_RegAddr     0xA1
#define BMP280_calib24_RegAddr     0xA0
#define BMP280_calib23_RegAddr     0x9F
#define BMP280_calib22_RegAddr     0x9E
#define BMP280_calib21_RegAddr     0x9D
#define BMP280_calib20_RegAddr     0x9C
#define BMP280_calib19_RegAddr     0x9B
#define BMP280_calib18_RegAddr     0x9A
#define BMP280_calib17_RegAddr     0x99
#define BMP280_calib16_RegAddr     0x98
#define BMP280_calib15_RegAddr     0x97
#define BMP280_calib14_RegAddr     0x96
#define BMP280_calib13_RegAddr     0x95
#define BMP280_calib12_RegAddr     0x94
#define BMP280_calib11_RegAddr     0x93
#define BMP280_calib10_RegAddr     0x92
#define BMP280_calib09_RegAddr     0x91
#define BMP280_calib08_RegAddr     0x90
#define BMP280_calib07_RegAddr     0x8F
#define BMP280_calib06_RegAddr     0x8E
#define BMP280_calib05_RegAddr     0x8D
#define BMP280_calib04_RegAddr     0x8C
#define BMP280_calib03_RegAddr     0x8B
#define BMP280_calib02_RegAddr     0x8A
#define BMP280_calib01_RegAddr     0x89
#define BMP280_calib00_RegAddr     0x88

/* ---------------------------- Mask ----------------------------- */

#define BMP280_ctrl_meas_osrs_t_Mask 0b11100000
#define BMP280_ctrl_meas_osrs_p_Mask 0b00011100
#define BMP280_ctrl_meas_mode_Mask   0b00000011

#define BMP280_config_t_sb_Mask      0b11100000
#define BMP280_config_filter_Mask    0b00011100
#define BMP280_config_spi3w_en_Mask  0b00000001

#define BMP280_status_measuring_Mask 0b00001000
#define BMP280_status_im_update_Mask 0b00000001

/* -------------------- Pressure measurement --------------------- */

  // ctrl_meas = [osrs_t(2:0)][osrs_p(2:0)][mode(1:0)]
  // ctrl_meas = [X][X][X][osrs_p(2)][osrs_p(1)][osrs_p(0)][X][X]
  //
  //                                  0b000___00

#define BMP280_PressureMeas_Skipped   0b00000000
#define BMP280_PressureMeas_x1        0b00000100
#define BMP280_PressureMeas_x2        0b00001000
#define BMP280_PressureMeas_x4        0b00001100
#define BMP280_PressureMeas_x8        0b00010000
#define BMP280_PressureMeas_x16       0b00011100

/* ------------------- Temperature measurement ------------------- */

  // ctrl_meas = [osrs_t(2:0)][osrs_p(2:0)][mode(1:0)]
  // ctrl_meas = [osrs_t(2)][osrs_t(1)][osrs_t(0)][X][X][X][X][X]
  //
  //                                    0b___00000

#define BMP280_TemperatureMeas_Skipped  0b00000000
#define BMP280_TemperatureMeas_x1       0b00100000
#define BMP280_TemperatureMeas_x2       0b01000000
#define BMP280_TemperatureMeas_x4       0b01100000
#define BMP280_TemperatureMeas_x8       0b10000000
#define BMP280_TemperatureMeas_x16      0b11100000

/* ----------------------- Power menagement ---------------------- */

// ctrl_meas = [osrs_t(2:0)][osrs_p(2:0)][mode(1:0)]
// ctrl_meas = [X][X][X][X][X][X][X][X]
//
//                           0b000000__

#define BMP280_Mode_Sleep    0b00000000
#define BMP280_Mode_Force    0b00000001
#define BMP280_Mode_Normal   0b00000011

/* --------------- Recomended oversampling settings -------------- */

// ctrl_meas = [osrs_t(2:0)][osrs_p(2:0)][mode(1:0)]

#define BMP280_RecommendedOver_PressureSkipped       0b11100000
#define BMP280_RecommendedOver_UltraLowPower         0b00100100
#define BMP280_RecommendedOver_LowPower              0b00101000
#define BMP280_RecommendedOver_StandardResolution    0b00101100
#define BMP280_RecommendedOver_HighResolution        0b00110000
#define BMP280_RecommendedOver_UltraHighResolution   0b01011100


/* ---------- Delays in Recommended oversampling settings -------- */

#define BMP280_RecommendedDelayms_UltraLowPower         7
#define BMP280_RecommendedDelayms_LowPower              9
#define BMP280_RecommendedDelayms_StandardResolution    14
#define BMP280_RecommendedDelayms_HighResolution        23
#define BMP280_RecommendedDelayms_UltraHighResolution   44

/* -------------------- Tstandby in Normal Mode ------------------ */

// control = [t_sb(2:0)][filter(2:0)][X][spi3w_en(0)]

#define BMP280_Tstandby_500us     0b00000000
#define BMP280_Tstandby_6250us    0b00100000
#define BMP280_Tstandby_125ms     0b01000000
#define BMP280_Tstandby_250ms     0b01100000
#define BMP280_Tstandby_500ms     0b10000000
#define BMP280_Tstandby_1000ms    0b10100000
#define BMP280_Tstandby_2000ms    0b11000000
#define BMP280_Tstandby_4000ms    0b11100000

/* -------------------------- IIR Filter ------------------------- */

// control = [t_sb(2:0)][filter(2:0)][X][spi3w_en(0)]

#define BMP280_IIRFilter_Off      0b00000000
#define BMP280_IIRFilter_2        0b00000100
#define BMP280_IIRFilter_4        0b00001000
#define BMP280_IIRFilter_8        0b00001100
#define BMP280_IIRFilter_16       0b00011100

/* ----------------------- 3-Wire SPI Mode ----------------------- */

#define BMP280_3WireSPI_Enable    0b00000001
#define BMP280_3WireSPI_Disable   0b00000000

/* ------------------------ Useful Macros ------------------------ */

#define BMP280_ReadRegisterMacro(Reg)  ((Reg & 0x7F) | 0x80)
#define BMP280_WriteRegisterMacro(Reg)  (Reg & 0x7F)
#define BMP280_SUCCES 1
#define BMP280_ERROR 0

/* *************************************************************** */
/* -------------------- Functions declarations ------------------- */
/* *************************************************************** */

status_t BMP280_init(SPI_HandleTypeDef* _hspi, GPIO_TypeDef* _GPIO_CSB, uint16_t _GPIO_Pin_CSB);
bool_t BMP280_CheckConnection();
status_t BMP280_Reset();
status_t BMP280_SetMeasurementOversamplingAndMode(uint8_t _PressureOver, uint8_t _TemperatureOver, uint8_t _Mode);
status_t BMP280_SetMode(uint8_t _Mode);
status_t BMP280_SetTemperatureOversampling(uint8_t _TemperatureOver);
status_t BMP280_SetPressureOversampling(uint8_t _PressureOver);
status_t BMP280_SetOversamplingOnRecommended(uint8_t _Over);
status_t BMP280_SetTstandby(uint8_t _Tstandby);
status_t BMP280_SetIIRFilter(uint8_t _IIRFilter);
status_t BMP280_Set3WireSPIMode(uint8_t _Mode);
status_t BMP280_Measure(int32_t* Temperature, uint32_t* Pressure);
bool_t BMP280_IsMeasuring();
bool_t BMP280_IsUpdating();
void BMP280_BlockingWait_Recommended();

void BMP280__StartTransmision();
void BMP280__StopTransmision();
status_t BMP280__ReadRegister(uint8_t* TxData, uint8_t* RxData, uint8_t size);
status_t BMP280__WriteRegister(uint8_t* TxData, uint8_t size);
int16_t BMP280__CountCompensateDigitSigned(int8_t lsb, int8_t msb);
uint16_t BMP280__CountCompensateDigitUnsigned(uint8_t lsb, uint8_t msb);
int32_t BMP280__CompensateTemperature(const int32_t* _ReadTemperature, int32_t* _pom);
uint32_t BMP280__CompensatePressure(const int32_t* _ReadPressure, int32_t* _pom);

bool_t BMP280_Check();

typedef struct{
  SPI_HandleTypeDef* hspi;
  GPIO_TypeDef* GPIO_CSB;
  uint16_t GPIO_Pin_CSB;

  volatile uint8_t Reg_temp_xlsb;
  volatile uint8_t Reg_temp_lsb;
  volatile uint8_t Reg_temp_msb;
  volatile uint8_t Reg_press_xlsb;
  volatile uint8_t Reg_press_lsb;
  volatile uint8_t Reg_press_msb;
  uint8_t Reg_ctrl_meas;
  uint8_t Reg_config;
  uint8_t Reg_calib[26];

  uint16_t compensate_dig_T1;
  int16_t compensate_dig_T2;
  int16_t compensate_dig_T3;
  uint16_t compensate_dig_P1;
  int16_t compensate_dig_P2;
  int16_t compensate_dig_P3;
  int16_t compensate_dig_P4;
  int16_t compensate_dig_P5;
  int16_t compensate_dig_P6;
  int16_t compensate_dig_P7;
  int16_t compensate_dig_P8;
  int16_t compensate_dig_P9;
} BMP280Struct;

extern BMP280Struct BMP280;



#endif /* BMP280_H_ */
