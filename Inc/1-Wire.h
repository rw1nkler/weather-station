/*
 * 1-Wire.h
 *
 *  Created on: 01.05.2018
 *      Author: robert
 */

#ifndef ONE_WIRE_H_
#define ONE_WIRE_H_

#include "gpio.h"
#include "tim.h"

#define ONEWIRE_TRANSMISSION_STATE_NONE                               0
#define ONEWIRE_TRANSMISSION_SUBSTATE_NONE                                 0
#define ONEWIRE_TRANSMISSION_STATE_INITIALIZATION                     1
  #define ONEWIRE_TRANSMISSION_INITIALIZATION_SUBSTATE_PULLUPLINE          11
  #define ONEWIRE_TRANSMISSION_INITIALIZATION_SUBSTATE_WATCHPRESENCEPULSES 12
  #define ONEWIRE_TRANSMISSION_INITIALIZATION_SUBSTATE_CHECKPRESENCE       13
#define ONEWIRE_TRANSMISSION_STATE_NEXT                               2
  #define ONEWIRE_TRANSMISSION_NEXT_SUBSTATE_NONE                          21
#define ONEWIRE_TRANSMISSION_STATE_SEND                               3
  #define ONEWIRE_TRANSMISSION_SEND_SUBSTATE_READBUFFER                    31
  #define ONEWIRE_TRANSMISSION_SEND_SUBSTATE_WRITE1SLOT                    32
  #define ONEWIRE_TRANSMISSION_SEND_SUBSTATE_WRITE0SLOT                    33
  #define ONEWIRE_TRANSMISSION_SEND_SUBSTATE_PULLUPLINE                    34
#define ONEWIRE_TRANSMISSION_STATE_READ                               4
  #define ONEWIRE_TRANSMISSION_READ_SUBSTATE_PULLDOWNLINE                  41
  #define ONEWIRE_TRANSMISSION_READ_SUBSTATE_PULLUPLINE                    42
  #define ONEWIRE_TRANSMISSION_READ_SUBSTATE_CHECKSTATE                    43
#define ONEWIRE_TRANSMISSION_STATE_END                                5
  #define ONEWIRE_TRANSMISSION_END_SUBSTATE_NONE                           51

typedef uint8_t status_t;
typedef uint8_t bool_t;
typedef uint8_t operation_t;

typedef struct{
  TIM_HandleTypeDef* htim_ptr;
  uint32_t timOCChannel;
  GPIO_TypeDef* GPIOx;
  uint16_t GPIO_Pin;

  void (*InitProcFailure_Callback)();
  void (*WriteEnd_Callback)();
  void (*EndOfTransmission_Callback)();
  void (*ReadEnd_Callback)();

  volatile uint8_t TransmissionState;
  volatile uint8_t TransmissionSubState;

  volatile uint8_t CheckPresenceTab[50];
  volatile uint8_t PresenceIterator;

  volatile uint8_t* SendBuffer;
  volatile uint16_t SendBufferIterator;
  volatile uint8_t SendBufferTabIterator;
  volatile uint8_t SendBufferTabSize;

  volatile uint64_t* ReadBuffer;
  uint8_t ReadBufferSize;
  volatile uint8_t ReadBufferIterator;

  uint8_t CurrentOperation[10];
  volatile uint8_t CurrentOperationIterator;
  volatile uint8_t CurrentOperationAmount;

  volatile uint8_t Buffer[2];

} OneWireStruct;

//extern OneWireStruct OneWire;  //

void OneWire_Init(OneWireStruct* OneWireInterface, TIM_HandleTypeDef* htim, uint32_t timOCChannel, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
void OneWire_WriteCommand_SkipROM_IT(OneWireStruct* OneWireInterface, volatile  uint8_t *TwoElementTabBuffer, uint8_t Command);
void OneWire_Read_SkipROM_IT(OneWireStruct* OneWireInterface, volatile uint64_t* ReadBuffer, volatile  uint8_t ReadBufferSize);

void OneWire_OC_DelayElapsedCallback_USE_IT_IN_CALLBACK(OneWireStruct* OneWireInterface);

void OneWire_SetInitProcFailure_Callback(OneWireStruct* OneWireInterface, void (*funPointer)());
void OneWire_SetWriteEnd_Callback(OneWireStruct* OneWireInterface, void (*funPointer)());
void OneWire_SetReadEnd_Callback(OneWireStruct* OneWireInterface, void (*funPointer)());
void OneWire_SetEndOfTransmission_Callback(OneWireStruct* OneWireInterface, void (*funPointer)());

uint32_t DS18B20_ReadTemp(uint16_t Measured);



void OneWire__InitializationProcedure(OneWireStruct* OneWireInterface);
void OneWire__SendBufferInit(OneWireStruct* OneWireInterface);
void OneWire__SetInterval_IT(OneWireStruct* OneWireInterface, uint32_t time_us);
void OneWire__ConfigurePinAsInput(OneWireStruct* OneWireInterface);
void OneWire__ConfigurePinAsOutput_HIGH(OneWireStruct* OneWireInterface);
void OneWire__ConfigurePinAsOutput_LOW(OneWireStruct* OneWireInterface);
bool_t OneWire__CheckPresence(OneWireStruct* OneWireInterface);
int16_t OneWire__FetchBuffer(OneWireStruct* OneWireInterface);
uint8_t OneWire__FetchOperation(OneWireStruct* OneWireInterface);
uint8_t OneWire__AddOperation(OneWireStruct* OneWireInterface, operation_t Operation);
void OneWire__DisplayOperationCircularBuffer(OneWireStruct* OneWireInterface);
void OneWire__PutReadBuffer(OneWireStruct* OneWireInterface, uint8_t Bit);
void OneWire__CleanReadBuffer(OneWireStruct* OneWireInterface);
void OneWire__CleanPresenceTab(OneWireStruct* OneWireInterface);
#endif /* 1_WIRE_H_ */
