/*
 * ESP8826.h
 *
 *  Created on: 06.04.2018
 *      Author: robert
 */

#ifndef ESP8826_H_
#define ESP8826_H_

#include "main.h"
#include "stm32f4xx_hal.h"
#include "usart.h"
#include "gpio.h"

typedef uint8_t bool_t;

typedef struct{
	UART_HandleTypeDef* huart;
	char* _Network;
	char* _Password;
	char* _ConnectionType;
	char* _ServerIP;
	char* _Port;

	bool_t _WiFiSettingsSet;
	bool_t _ServerSettingsSet;

	bool_t FatalError;

} ESP8826Struct;

typedef uint8_t bool_t;
typedef uint8_t status_t;

#define ESP8826_SUCCESS 1
#define ESP8826_ERROR   0

#define ESP8826_Mode_Client       '1'
#define ESP8826_Mode_Server       '2'
#define ESP8826_Mode_ClientServer '3'

#define ESP8826_STATUS_CONNECTED_TO_AP                  '2'
#define ESP8826_STATUS_CREATED_TCP_OR_UDP_TRANSMISSION  '3'
#define ESP8826_STATUS_DISCONNESTED_FROM_TCP_UDP        '4'
#define ESP8826_STATUS_DISCONNECTED_FROM_AP             '5'


status_t ESP8826_Init(ESP8826Struct* ESP8826, UART_HandleTypeDef* huart);
bool_t ESP8826_CheckConnection(ESP8826Struct* ESP8826);
void ESP8826_SetWiFiSettings(ESP8826Struct* ESP8826, const char* Network, const char* Password);
void ESP8826_SetServerSettings(ESP8826Struct* ESP8826, const char* type, const char* ip, const char* port);
//status_t ESP8826_ReadFirmwareVersion(ESP8826Struct* ESP8826, char** Firmware, uint8_t* size);
status_t ESP8826_SetMode(ESP8826Struct* ESP8826, uint8_t _Mode);
status_t ESP8826_ConnectWithWiFi(ESP8826Struct* ESP8826);
status_t ESP8826_DeepSleepModeEnable(ESP8826Struct* ESP8826, uint16_t time_ms);
status_t ESP8826_SendGet(ESP8826Struct* ESP8826, const char* message);
uint8_t ESP8826_CheckConnectionStatus(ESP8826Struct* ESP8826);

status_t ESP8826__ConnectWithServer(ESP8826Struct* ESP8826);

status_t ESP8826_DESTRUCT(ESP8826Struct* ESP8826);




#endif /* ESP8826_H_ */
