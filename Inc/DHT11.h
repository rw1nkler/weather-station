/*
 * DHT11.h
 *
 *  Created on: 09.06.2018
 *      Author: robert
 */

#ifndef DHT11_H_
#define DHT11_H_

#include "gpio.h"
#include "tim.h"

#define DHT11_TRANSMISSION_STATE_NONE                               0
#define DHT11_TRANSMISSION_STATE_PULLUPLINE                         1
#define DHT11_TRANSMISSION_STATE_STARTREAD                          2
#define DHT11_TRANSMISSION_STATE_END                                3

typedef struct{
  TIM_HandleTypeDef* htim_ptr;
  uint32_t timOCChannel;
  uint32_t timICChannel;
  GPIO_TypeDef* GPIOx;
  uint16_t GPIO_Pin;

  void (*EndOfTransmission_Callback)();

  volatile uint32_t ReadBuffer;
  volatile uint8_t ReadBufferIterator;
  volatile uint32_t TickCounter;

  volatile uint8_t TransmissionState;


} DHT11Struct;

void DHT11_Init(DHT11Struct* DHT11, TIM_HandleTypeDef* htim, uint32_t timOCChannel, uint32_t timICChannel, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin);
void DHT11__SetInterval_IT(DHT11Struct* DHT11, uint32_t time_us);

void DHT11_OC_DelayElapsedCallback_USE_IT_IN_CALLBACK(DHT11Struct* DHT11);
void DHT11_IC_CaptureCallback_USE_IT_IN_CALLBACK(DHT11Struct* DHT11);

void DHT11_SetEndOfTransmission_Callback(DHT11Struct* DHT11, void (*funPointer)());

uint32_t DHT11_ReadTemperature(DHT11Struct* DHT11);
uint32_t DHT11_ReadHumidity(DHT11Struct* DHT11);

void DHT11__ConfigurePinAsInput(DHT11Struct* DHT11);
void DHT11__ConfigurePinAsOutput_HIGH(DHT11Struct* DHT11);
void DHT11__ConfigurePinAsOutput_LOW(DHT11Struct* DHT11);
void DHT11__InitializationProcedure(DHT11Struct* DHT11);


#endif /* DHT11_H_ */
