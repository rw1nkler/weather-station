/**
 ******************************************************************************
 * @file           : main.c
 * @brief          : Main program body
 ******************************************************************************
 ** This notice applies to any and all portions of this file
 * that are not between comment pairs USER CODE BEGIN and
 * USER CODE END. Other portions of this file, whether
 * inserted by the user or by software development tools
 * are owned by their respective copyright owners.
 *
 * COPYRIGHT(c) 2018 STMicroelectronics
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted provided that the following conditions are met:
 *   1. Redistributions of source code must retain the above copyright notice,
 *      this list of conditions and the following disclaimer.
 *   2. Redistributions in binary form must reproduce the above copyright notice,
 *      this list of conditions and the following disclaimer in the documentation
 *      and/or other materials provided with the distribution.
 *   3. Neither the name of STMicroelectronics nor the names of its contributors
 *      may be used to endorse or promote products derived from this software
 *      without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE
 * DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 *
 ******************************************************************************
 */
/* Includes ------------------------------------------------------------------*/
#include "main.h"
#include "stm32f4xx_hal.h"
#include "spi.h"
#include "tim.h"
#include "usart.h"
#include "gpio.h"

/* USER CODE BEGIN Includes */
#include <stdio.h>
#include <string.h>
#include "BMP280.h"
#include "ESP8826.h"
#include "1-Wire.h"
#include "DHT11.h"
/* USER CODE END Includes */

/* Private variables ---------------------------------------------------------*/

/* USER CODE BEGIN PV */
/* Private variables ---------------------------------------------------------*/
volatile int32_t BMP280Temp;
volatile uint32_t BMP280Cisn;
volatile uint32_t DS18B20Temp;
volatile uint32_t DHT11Temp;
volatile uint32_t DHT11Wilg;

ESP8826Struct ESP8826_1;
OneWireStruct OneWire;
DHT11Struct DHT11;

volatile uint8_t MeasureFLAG = 1;
volatile uint8_t Buffer[2];
volatile uint64_t Read;

/* Dodanie do one wire */
/* USER CODE END PV */

/* Private function prototypes -----------------------------------------------*/
void SystemClock_Config(void);

/* USER CODE BEGIN PFP */
/* Private function prototypes -----------------------------------------------*/
void EndOfTransmissionCallbackDS18B20();
void EndOfTransmissionCallbackDHT11();

/* USER CODE END PFP */

/* USER CODE BEGIN 0 */

/* USER CODE END 0 */

/**
 * @brief  The application entry point.
 *
 * @retval None
 */
int main(void) {
	/* USER CODE BEGIN 1 */
	/* USER CODE END 1 */

	/* MCU Configuration----------------------------------------------------------*/

	/* Reset of all peripherals, Initializes the Flash interface and the Systick. */
	HAL_Init();

	/* USER CODE BEGIN Init */

	/* USER CODE END Init */

	/* Configure the system clock */
	SystemClock_Config();

	/* USER CODE BEGIN SysInit */

	/* USER CODE END SysInit */

	/* Initialize all configured peripherals */
	MX_GPIO_Init();
	MX_USART2_UART_Init();
	MX_SPI2_Init();
	MX_USART1_UART_Init();
	MX_TIM3_Init();
	MX_TIM5_Init();
	MX_TIM2_Init();
	/* USER CODE BEGIN 2 */

	printf("BMP280 Init: %d \r\n", BMP280_init(&hspi2, SPI2_CSB_GPIO_Port, SPI2_CSB_Pin));
	printf("BMP280 Filter: %d \r\n", BMP280_SetIIRFilter(BMP280_IIRFilter_16));

	printf("ESP8826 Init: %d \r\n", ESP8826_Init(&ESP8826_1, &huart1));
	printf("ESP8826 SetMode: %d \r\n", ESP8826_SetMode(&ESP8826_1, ESP8826_Mode_Client));
	ESP8826_SetWiFiSettings(&ESP8826_1, "FunBox2-6769", "QP10TY#97");
	ESP8826_SetServerSettings(&ESP8826_1, "TCP", "192.168.1.20", "80");
	printf("ESP8826 WifiConnect: %d \r\n", ESP8826_ConnectWithWiFi(&ESP8826_1));

	OneWire_Init(&OneWire, &htim3, TIM_CHANNEL_1, OneWireBus_GPIO_Port, OneWireBus_Pin);
	OneWire_SetEndOfTransmission_Callback(&OneWire, EndOfTransmissionCallbackDS18B20);

	DHT11_Init(&DHT11, &htim2, TIM_CHANNEL_1, TIM_CHANNEL_2, DHT11_GPIO_Port, DHT11_Pin);
	DHT11_SetEndOfTransmission_Callback(&DHT11, EndOfTransmissionCallbackDHT11);

	HAL_Delay(100);

	HAL_TIM_Base_Start_IT(&htim5);
	/* USER CODE END 2 */

	/* Infinite loop */
	/* USER CODE BEGIN WHILE */
	while (1){

		/* USER CODE END WHILE */

		/* USER CODE BEGIN 3 */

		if (MeasureFLAG == 1) {
			uint8_t errorBMP280Temp = 0;
			uint8_t errorBMP280Cisn = 0;
			uint8_t errorDS18B20Temp = 0;
			uint8_t errorDHT11Temp = 0;
			uint8_t errorDHT11Wilg = 0;

			OneWire_WriteCommand_SkipROM_IT(&OneWire, Buffer, 0x44);
			printf("BMP280 Set Mode: %d \r\n", BMP280_SetMeasurementOversamplingAndMode( BMP280_PressureMeas_x16, BMP280_TemperatureMeas_x2, BMP280_Mode_Force));
			HAL_Delay(100);
			DHT11__InitializationProcedure(&DHT11);
			HAL_Delay(100);


			printf("BMP280 Measure: %d \r\n", BMP280_Measure(&BMP280Temp, &BMP280Cisn));
			printf("BMP280Temp: %ld\r\n", BMP280Temp);
			printf("BMP280Cisn: %ld\r\n", BMP280Cisn);
			printf("DS18B20Temp: %ld \r\n", DS18B20Temp);
			printf("DHT11Temp: %ld\r\n", DHT11Temp);
			printf("DHT11Wilg: %ld \r\n", DHT11Wilg);

			char buffer[200];
			memset(buffer, '\0', 200);
			sprintf(buffer, "/pages/receive/receive.php?CzujnikID=15&"
					"CzujnikHaslo=haslo&BMP280TEMP=%ld", BMP280Temp);
			printf("%s \r\n", buffer);

			while (!ESP8826_SendGet(&ESP8826_1, buffer)) {
				printf("ESP8826 Send Get BMP280Temp: 0 \r\n");
				if (errorBMP280Temp >= 3)
					break;
				++errorBMP280Temp;
				HAL_Delay(60000);
			}
			if (errorBMP280Temp < 3)
				printf("ESP8826 Send Get BMP280Temp: 1 \r\n");
			HAL_Delay(100);

			memset(buffer, '\0', 200);
			sprintf(buffer, "/pages/receive/receive.php?CzujnikID=15&"
					"CzujnikHaslo=haslo&BMP280CISN=%lu", BMP280Cisn);
			printf("%s \r\n", buffer);
			while (!ESP8826_SendGet(&ESP8826_1, buffer)) {
				printf("ESP8826 Send Get BMP280Cisn: 0 \r\n");
				if (errorBMP280Cisn >= 3)
					break;
				++errorBMP280Cisn;
				HAL_Delay(60000);
			}
			if (errorBMP280Cisn < 3)
				printf("ESP8826 Send Get BMP280Cisn: 1 \r\n");
			HAL_Delay(100);

			memset(buffer, '\0', 200);
			sprintf(buffer, "/pages/receive/receive.php?CzujnikID=15&"
					"CzujnikHaslo=haslo&DS18B20TEM=%lu", DS18B20Temp);
			printf("%s \r\n", buffer);
			while (!ESP8826_SendGet(&ESP8826_1, buffer)) {
				printf("ESP8826 Send Get DS18B20Temp: 0 \r\n");
				if (errorDS18B20Temp >= 3)
					break;
				++errorDS18B20Temp;
				HAL_Delay(60000);
			}
			if (errorDS18B20Temp < 3)
				printf("ESP8826 Send Get DS18B20Temp: 1 \r\n");
			HAL_Delay(100);

			memset(buffer, '\0', 200);
			sprintf(buffer, "/pages/receive/receive.php?CzujnikID=15&"
					"CzujnikHaslo=haslo&DHT11TEMP=%lu", DHT11Temp);
			printf("%s \r\n", buffer);
			while (!ESP8826_SendGet(&ESP8826_1, buffer)) {
				printf("ESP8826 Send Get DHT11Temp: 0 \r\n");
				if (errorDHT11Temp >= 3)
					break;
				++errorDHT11Temp;
				HAL_Delay(60000);
			}
			if (errorDHT11Temp < 3)
				printf("ESP8826 Send Get DHT11Temp: 1 \r\n");
			HAL_Delay(100);

			memset(buffer, '\0', 200);
			sprintf(buffer, "/pages/receive/receive.php?CzujnikID=15&"
					"CzujnikHaslo=haslo&DHT11WILG=%lu", DHT11Wilg);
			printf("%s \r\n", buffer);
			while (!ESP8826_SendGet(&ESP8826_1, buffer)) {
				printf("ESP8826 Send Get DHT11Wilg: 0 \r\n");
				if (errorDHT11Wilg >= 3)
					break;
				++errorDHT11Wilg;
				HAL_Delay(60000);
			}
			if (errorDHT11Wilg < 3)
				printf("ESP8826 Send Get DHT11Wilg: 1 \r\n");
			HAL_Delay(100);

			MeasureFLAG = 0;
		}
		HAL_Delay(10);

	}
	/* USER CODE END 3 */

}

/**
 * @brief System Clock Configuration
 * @retval None
 */
void SystemClock_Config(void) {

	RCC_OscInitTypeDef RCC_OscInitStruct;
	RCC_ClkInitTypeDef RCC_ClkInitStruct;

	/**Configure the main internal regulator output voltage
	 */
	__HAL_RCC_PWR_CLK_ENABLE()
	;

	__HAL_PWR_VOLTAGESCALING_CONFIG(PWR_REGULATOR_VOLTAGE_SCALE3);

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_OscInitStruct.OscillatorType = RCC_OSCILLATORTYPE_HSI;
	RCC_OscInitStruct.HSIState = RCC_HSI_ON;
	RCC_OscInitStruct.HSICalibrationValue = 16;
	RCC_OscInitStruct.PLL.PLLState = RCC_PLL_ON;
	RCC_OscInitStruct.PLL.PLLSource = RCC_PLLSOURCE_HSI;
	RCC_OscInitStruct.PLL.PLLM = 8;
	RCC_OscInitStruct.PLL.PLLN = 80;
	RCC_OscInitStruct.PLL.PLLP = RCC_PLLP_DIV2;
	RCC_OscInitStruct.PLL.PLLQ = 2;
	RCC_OscInitStruct.PLL.PLLR = 2;
	if (HAL_RCC_OscConfig(&RCC_OscInitStruct) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Initializes the CPU, AHB and APB busses clocks
	 */
	RCC_ClkInitStruct.ClockType = RCC_CLOCKTYPE_HCLK | RCC_CLOCKTYPE_SYSCLK
			| RCC_CLOCKTYPE_PCLK1 | RCC_CLOCKTYPE_PCLK2;
	RCC_ClkInitStruct.SYSCLKSource = RCC_SYSCLKSOURCE_PLLCLK;
	RCC_ClkInitStruct.AHBCLKDivider = RCC_SYSCLK_DIV1;
	RCC_ClkInitStruct.APB1CLKDivider = RCC_HCLK_DIV8;
	RCC_ClkInitStruct.APB2CLKDivider = RCC_HCLK_DIV2;

	if (HAL_RCC_ClockConfig(&RCC_ClkInitStruct, FLASH_LATENCY_2) != HAL_OK) {
		_Error_Handler(__FILE__, __LINE__);
	}

	/**Configure the Systick interrupt time
	 */
	HAL_SYSTICK_Config(HAL_RCC_GetHCLKFreq() / 1000);

	/**Configure the Systick
	 */
	HAL_SYSTICK_CLKSourceConfig(SYSTICK_CLKSOURCE_HCLK);

	/* SysTick_IRQn interrupt configuration */
	HAL_NVIC_SetPriority(SysTick_IRQn, 0, 0);
}

/* USER CODE BEGIN 4 */
int _write(int file, char* p, int length) {
	HAL_UART_Transmit(&huart2, (uint8_t*) p, length, 50);
	return length;
}

void HAL_TIM_PeriodElapsedCallback(TIM_HandleTypeDef *htim) {
	if (htim == &htim5) {
		MeasureFLAG = 1;
		HAL_GPIO_TogglePin(LD2_GPIO_Port, LD2_Pin);
	}
}

void HAL_TIM_IC_CaptureCallback(TIM_HandleTypeDef *htim) {
	DHT11_IC_CaptureCallback_USE_IT_IN_CALLBACK(&DHT11);
}

void HAL_TIM_OC_DelayElapsedCallback(TIM_HandleTypeDef *htim) {
	if (htim == OneWire.htim_ptr)
		OneWire_OC_DelayElapsedCallback_USE_IT_IN_CALLBACK(&OneWire);
	else if (htim == DHT11.htim_ptr)
		DHT11_OC_DelayElapsedCallback_USE_IT_IN_CALLBACK(&DHT11);
}

void EndOfTransmissionCallbackDHT11() {
	DHT11Wilg = DHT11_ReadHumidity(&DHT11);
	DHT11Temp = DHT11_ReadTemperature(&DHT11);
}

void EndOfTransmissionCallbackDS18B20() {
	static volatile uint8_t state = 0;
	if (state == 0) {
		OneWire_Read_SkipROM_IT(&OneWire, &Read, 16);
		++state;
	} else if (state == 1) {
		DS18B20Temp = DS18B20_ReadTemp((uint16_t) Read);
		state = 0;
	}
}

/* USER CODE END 4 */

/**
 * @brief  This function is executed in case of error occurrence.
 * @param  file: The file name as string.
 * @param  line: The line in file as a number.
 * @retval None
 */
void _Error_Handler(char *file, int line) {
	/* USER CODE BEGIN Error_Handler_Debug */
	/* User can add his own implementation to report the HAL error return state */
	while (1) {
	}
	/* USER CODE END Error_Handler_Debug */
}

#ifdef  USE_FULL_ASSERT
/**
 * @brief  Reports the name of the source file and the source line number
 *         where the assert_param error has occurred.
 * @param  file: pointer to the source file name
 * @param  line: assert_param error line source number
 * @retval None
 */
void assert_failed(uint8_t* file, uint32_t line)
{
	/* USER CODE BEGIN 6 */
	/* User can add his own implementation to report the file name and line number,
	 tex: printf("Wrong parameters value: file %s on line %d\r\n", file, line) */
	/* USER CODE END 6 */
}
#endif /* USE_FULL_ASSERT */

/**
 * @}
 */

/**
 * @}
 */

/************************ (C) COPYRIGHT STMicroelectronics *****END OF FILE****/
