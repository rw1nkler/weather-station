/*
 * BMP280.c
 *
 *  Created on: 17.03.2018
 *      Author: robert
 */

#include "BMP280.h"

BMP280Struct BMP280;

void BMP280__StartTransmision(){
  HAL_GPIO_WritePin(BMP280.GPIO_CSB, BMP280.GPIO_Pin_CSB, GPIO_PIN_RESET);
  HAL_Delay(1);
}

void BMP280__StopTransmision(){
  HAL_GPIO_WritePin(BMP280.GPIO_CSB, BMP280.GPIO_Pin_CSB, GPIO_PIN_SET);
  HAL_Delay(1);
}

status_t BMP280__ReadRegister(uint8_t* TxData, uint8_t* RxData, uint8_t size){
  uint8_t Error = 0;

  BMP280__StartTransmision();
    Error += HAL_SPI_Transmit(BMP280.hspi, TxData, sizeof(uint8_t), 50);
    Error += HAL_SPI_Receive(BMP280.hspi, RxData, size, 50);
  BMP280__StopTransmision();

  return (Error == 0) ? 0 : 1;
}

status_t BMP280__WriteRegister(uint8_t* TxData, uint8_t size){
  uint8_t Error = 0;

  BMP280__StartTransmision();
      Error += HAL_SPI_Transmit(BMP280.hspi, TxData, size, 50);
  BMP280__StopTransmision();

  return (Error == 0) ? 0 : 1;

}

bool_t BMP280_CheckConnection(){
  uint8_t TxData;
  uint8_t RxData;
  bool_t Error = 0;

  TxData = BMP280_ReadRegisterMacro(BMP280_id_RegAddr);
  Error = BMP280__ReadRegister(&TxData, &RxData, sizeof(uint8_t));

  return (RxData == 0x58 && Error == 0) ? BMP280_SUCCES : BMP280_ERROR;
}

status_t BMP280_Reset(SPI_HandleTypeDef *hspi){
  uint8_t TxData[2];
  bool_t Error = 0;

  TxData[0] = BMP280_WriteRegisterMacro(BMP280_reset_RegAddr);
  TxData[1] = 0xB6;
  Error = BMP280__WriteRegister((uint8_t*) &TxData, 16);

  return (Error == 0) ? BMP280_SUCCES : BMP280_ERROR;
}

status_t BMP280_SetMeasurementOversamplingAndMode(uint8_t _PressureOver, uint8_t _TemperatureOver, uint8_t _Mode){
  uint8_t TxData[2];
  uint8_t RxData;
  bool_t Error = 0;
  TxData[0] = BMP280_WriteRegisterMacro(BMP280_ctrl_meas_RegAddr);
  TxData[1] = (_PressureOver | _TemperatureOver | _Mode);

  Error += BMP280__WriteRegister((uint8_t*) &TxData, 16);

  TxData[0] = BMP280_ReadRegisterMacro(BMP280_ctrl_meas_RegAddr);
  Error += BMP280__ReadRegister((uint8_t*) &TxData, &RxData, sizeof(uint8_t));

  if(Error == 0 && RxData == TxData[1]){
	  BMP280.Reg_ctrl_meas = RxData;
	  return BMP280_SUCCES;
  }
  else return BMP280_ERROR;
}

status_t BMP280_SetTemperatureOversampling(uint8_t _TemperatureOver){
  uint8_t TxData[2];
  uint8_t RxData;
  bool_t Error = 0;
  TxData[0] = BMP280_WriteRegisterMacro(BMP280_ctrl_meas_RegAddr);
  TxData[1] = ((_TemperatureOver)
		     |(BMP280.Reg_ctrl_meas & BMP280_ctrl_meas_osrs_p_Mask)
		     |(BMP280.Reg_ctrl_meas & BMP280_ctrl_meas_mode_Mask));

  Error += BMP280__WriteRegister((uint8_t*) &TxData, 16);

  TxData[0] = BMP280_ReadRegisterMacro(BMP280_ctrl_meas_RegAddr);
  Error += BMP280__ReadRegister((uint8_t*) &TxData, &RxData, sizeof(uint8_t));

  if(Error == 0 && RxData == TxData[1]){
	  BMP280.Reg_ctrl_meas = RxData;
	  return BMP280_SUCCES;
  }
  else return BMP280_ERROR;
}

status_t BMP280_SetPressureOversampling(uint8_t _PressureOver){
  uint8_t TxData[2];
  uint8_t RxData;
  bool_t Error = 0;
  TxData[0] = BMP280_WriteRegisterMacro(BMP280_ctrl_meas_RegAddr);
  TxData[1] = ((BMP280.Reg_ctrl_meas & BMP280_ctrl_meas_osrs_t_Mask)
		     |(_PressureOver)
		     |(BMP280.Reg_ctrl_meas & BMP280_ctrl_meas_mode_Mask));

  Error += BMP280__WriteRegister((uint8_t*) &TxData, 16);

  TxData[0] = BMP280_ReadRegisterMacro(BMP280_ctrl_meas_RegAddr);
  Error += BMP280__ReadRegister((uint8_t*) &TxData, &RxData, sizeof(uint8_t));

  if(Error == 0 && RxData == TxData[1]){
	  BMP280.Reg_ctrl_meas = RxData;
	  return BMP280_SUCCES;
  }
  else return BMP280_ERROR;
}

status_t BMP280_SetMode(uint8_t _Mode){
  uint8_t TxData[2];
  uint8_t RxData;
  bool_t Error = 0;
  TxData[0] = BMP280_WriteRegisterMacro(BMP280_ctrl_meas_RegAddr);
  TxData[1] = ((BMP280.Reg_ctrl_meas & BMP280_ctrl_meas_osrs_t_Mask)
		     |(BMP280.Reg_ctrl_meas & BMP280_ctrl_meas_osrs_p_Mask)
		     |(_Mode));

  Error += BMP280__WriteRegister((uint8_t*) &TxData, 16);

  TxData[0] = BMP280_ReadRegisterMacro(BMP280_ctrl_meas_RegAddr);
  Error += BMP280__ReadRegister((uint8_t*) &TxData, &RxData, sizeof(uint8_t));

  if(Error == 0 && RxData == TxData[1]){
	  BMP280.Reg_ctrl_meas = RxData;
	  return BMP280_SUCCES;
  }
  else return BMP280_ERROR;
}

status_t BMP280_init(SPI_HandleTypeDef* _hspi, GPIO_TypeDef* _GPIO_CSB, uint16_t _GPIO_Pin_CSB){
  BMP280.hspi = _hspi;
  BMP280.GPIO_CSB = _GPIO_CSB;
  BMP280.GPIO_Pin_CSB = _GPIO_Pin_CSB;

  BMP280.Reg_temp_xlsb  = 0x00;
  BMP280.Reg_temp_lsb   = 0x00;
  BMP280.Reg_temp_msb   = 0x80;
  BMP280.Reg_press_xlsb = 0x00;
  BMP280.Reg_press_lsb  = 0x00;
  BMP280.Reg_press_msb  = 0x80;
  BMP280.Reg_config     = 0x00;
  BMP280.Reg_ctrl_meas  = 0x00;

  uint8_t TxData;
  bool_t Error = 0;

  TxData = BMP280_ReadRegisterMacro(BMP280_calib00_RegAddr);
  BMP280__StartTransmision();
    Error += HAL_SPI_Transmit(BMP280.hspi, &TxData, sizeof(uint8_t), 50);
  for(int i = 0; i < 26; ++i)
    Error += HAL_SPI_Receive(BMP280.hspi, &(BMP280.Reg_calib[i]), sizeof(uint8_t), 50);
  BMP280__StopTransmision();

  BMP280.compensate_dig_T1 = BMP280__CountCompensateDigitUnsigned(BMP280.Reg_calib[0], BMP280.Reg_calib[1]);
  BMP280.compensate_dig_T2 = BMP280__CountCompensateDigitSigned(BMP280.Reg_calib[2], BMP280.Reg_calib[3]);
  BMP280.compensate_dig_T3 = BMP280__CountCompensateDigitSigned(BMP280.Reg_calib[4], BMP280.Reg_calib[5]);
  BMP280.compensate_dig_P1 = BMP280__CountCompensateDigitUnsigned(BMP280.Reg_calib[6], BMP280.Reg_calib[7]);
  BMP280.compensate_dig_P2 = BMP280__CountCompensateDigitSigned(BMP280.Reg_calib[8], BMP280.Reg_calib[9]);
  BMP280.compensate_dig_P3 = BMP280__CountCompensateDigitSigned(BMP280.Reg_calib[10], BMP280.Reg_calib[11]);
  BMP280.compensate_dig_P4 = BMP280__CountCompensateDigitSigned(BMP280.Reg_calib[12], BMP280.Reg_calib[13]);
  BMP280.compensate_dig_P5 = BMP280__CountCompensateDigitSigned(BMP280.Reg_calib[14], BMP280.Reg_calib[15]);
  BMP280.compensate_dig_P6 = BMP280__CountCompensateDigitSigned(BMP280.Reg_calib[16], BMP280.Reg_calib[17]);
  BMP280.compensate_dig_P7 = BMP280__CountCompensateDigitSigned(BMP280.Reg_calib[18], BMP280.Reg_calib[19]);
  BMP280.compensate_dig_P8 = BMP280__CountCompensateDigitSigned(BMP280.Reg_calib[20], BMP280.Reg_calib[21]);
  BMP280.compensate_dig_P9 = BMP280__CountCompensateDigitSigned(BMP280.Reg_calib[22], BMP280.Reg_calib[23]);

  HAL_GPIO_WritePin(_GPIO_CSB, _GPIO_Pin_CSB, GPIO_PIN_SET);
  return (Error == 0) ? BMP280_SUCCES : BMP280_ERROR;

}

status_t BMP280_SetOversamplingOnRecommended(uint8_t _Over){
  uint8_t TxData[2];
  uint8_t RxData;
  bool_t Error = 0;
  TxData[0] = BMP280_WriteRegisterMacro(BMP280_ctrl_meas_RegAddr);
  TxData[1] = ((_Over) | (BMP280.Reg_ctrl_meas & BMP280_ctrl_meas_mode_Mask));

  Error += BMP280__WriteRegister((uint8_t*) &TxData, 16);

  TxData[0] = BMP280_ReadRegisterMacro(BMP280_ctrl_meas_RegAddr);
  Error += BMP280__ReadRegister((uint8_t*) &TxData, &RxData, sizeof(uint8_t));

  if(Error == 0 && RxData == TxData[1]){
    BMP280.Reg_ctrl_meas = RxData;
    return BMP280_SUCCES;
  }
  else return BMP280_ERROR;
}

status_t BMP280_SetTstandby(uint8_t _Tstandby){
  uint8_t TxData[2];
  uint8_t RxData;
  bool_t Error = 0;
  TxData[0] = BMP280_WriteRegisterMacro(BMP280_config_RegAddr);
  TxData[1] = ((_Tstandby)
			  |(BMP280.Reg_config & BMP280_config_filter_Mask)
			  |(BMP280.Reg_config & BMP280_config_spi3w_en_Mask));

  Error += BMP280__WriteRegister((uint8_t*) &TxData, 16);

  TxData[0] = BMP280_ReadRegisterMacro(BMP280_config_RegAddr);
  Error += BMP280__ReadRegister((uint8_t*) &TxData, &RxData, sizeof(uint8_t));

  if(Error == 0 && RxData == TxData[1]){
	BMP280.Reg_config = RxData;
    return BMP280_SUCCES;
  }
  else return BMP280_ERROR;
}

status_t BMP280_SetIIRFilter(uint8_t _IIRFilter){
  uint8_t TxData[2];
  uint8_t RxData;
  bool_t Error = 0;
  TxData[0] = BMP280_WriteRegisterMacro(BMP280_config_RegAddr);
  TxData[1] = ((BMP280.Reg_config & BMP280_config_t_sb_Mask)
			  |(_IIRFilter)
			  |(BMP280.Reg_config & BMP280_config_spi3w_en_Mask));

  Error += BMP280__WriteRegister((uint8_t*) &TxData, 16);

  TxData[0] = BMP280_ReadRegisterMacro(BMP280_config_RegAddr);
  Error += BMP280__ReadRegister((uint8_t*) &TxData, &RxData, sizeof(uint8_t));

  if(Error == 0 && RxData == TxData[1]){
	BMP280.Reg_config = RxData;
    return BMP280_SUCCES;
  }
  else return BMP280_ERROR;
}

status_t BMP280_Set3WireSPIMode(uint8_t _Mode){
  uint8_t TxData[2];
  uint8_t RxData;
  bool_t Error = 0;
  TxData[0] = BMP280_WriteRegisterMacro(BMP280_config_RegAddr);
  TxData[1] = ((BMP280.Reg_config & BMP280_config_t_sb_Mask)
			  |(BMP280.Reg_config & BMP280_config_filter_Mask)
			  |(_Mode));

  Error += BMP280__WriteRegister((uint8_t*) &TxData, 16);

  TxData[0] = BMP280_ReadRegisterMacro(BMP280_config_RegAddr);
  Error += BMP280__ReadRegister((uint8_t*) &TxData, &RxData, sizeof(uint8_t));

  if(Error == 0 && RxData == TxData[1]){
	BMP280.Reg_config = RxData;
    return BMP280_SUCCES;
  }
  else return BMP280_ERROR;
}

bool_t BMP280_IsMeasuring(){
  uint8_t TxData;
  uint8_t RxData;

  TxData = BMP280_ReadRegisterMacro(BMP280_status_RegAddr);
  BMP280__ReadRegister(&TxData, &RxData, sizeof(uint8_t));

  return (RxData & BMP280_status_measuring_Mask);
}

bool_t BMP280_IsUpdating(){
  uint8_t TxData;
  uint8_t RxData;

  TxData = BMP280_ReadRegisterMacro(BMP280_status_RegAddr);
  BMP280__ReadRegister(&TxData, &RxData, sizeof(uint8_t));

  return (RxData & BMP280_status_im_update_Mask);
}

void BMP280_BlockingWait_Recommended(){
  uint8_t settings = (BMP280.Reg_ctrl_meas & (BMP280_ctrl_meas_osrs_t_Mask | BMP280_ctrl_meas_osrs_p_Mask));
  uint8_t delay;
  switch(settings){
    case BMP280_RecommendedOver_UltraLowPower:
    	delay = BMP280_RecommendedDelayms_UltraLowPower;
    	break;
    case BMP280_RecommendedOver_LowPower:
    	delay = BMP280_RecommendedDelayms_LowPower;
    	break;
    case BMP280_RecommendedOver_StandardResolution:
    	delay = BMP280_RecommendedDelayms_StandardResolution;
    	break;
    case BMP280_RecommendedOver_HighResolution:
    	delay = BMP280_RecommendedDelayms_HighResolution;
    	break;
    case BMP280_RecommendedOver_UltraHighResolution:
    	delay = BMP280_RecommendedDelayms_UltraHighResolution;
    	break;
    default :
    	delay = 50;
    	break;
  }
  HAL_Delay(delay);
}

int16_t BMP280__CountCompensateDigitSigned(int8_t lsb, int8_t msb){
  int16_t changed;
  int8_t* tmp = (int8_t*) &changed;
  tmp[0] = lsb;
  tmp[1] = msb;
  return changed;
}

uint16_t BMP280__CountCompensateDigitUnsigned(uint8_t lsb, uint8_t msb){
  uint16_t changed;
  uint8_t* tmp = (uint8_t*) &changed;
  tmp[0] = lsb;
  tmp[1] = msb;
  return changed;
}

int32_t BMP280__CompensateTemperature(const int32_t* _ReadTemperature, int32_t* _pom){
  int32_t var1t, var2t;
  int32_t pom;
  var1t = (((((*_ReadTemperature) >> 3) - ((int32_t) BMP280.compensate_dig_T1 << 1))) * ((int32_t) BMP280.compensate_dig_T2)) >> 11;
  var2t = ((((((*_ReadTemperature) >> 4) - ((int32_t) BMP280.compensate_dig_T1)) * (((*_ReadTemperature) >> 4) - ((int32_t) BMP280.compensate_dig_T1))) >> 12) * ((int32_t) BMP280.compensate_dig_T3)) >> 14;
  pom = var1t + var2t;
  if(_pom != NULL){
    *_pom = pom;
  }
  return ((pom * 5 + 128) >> 8);
}

uint32_t BMP280__CompensatePressure(const int32_t* _ReadPressure, int32_t* _pom){
  int64_t var1p, var2p, p;
  var1p = ((int64_t) (*_pom)) - 128000;
  var2p = var1p * var1p * (int64_t) BMP280.compensate_dig_P6;
  var2p = var2p + ((var1p * (int64_t) BMP280.compensate_dig_P5) << 17);
  var2p = var2p + (((int64_t) BMP280.compensate_dig_P4) << 35);
  var1p = ((var1p * var1p * (int64_t) BMP280.compensate_dig_P3) >> 8) + ((var1p * (int64_t) BMP280.compensate_dig_P2) << 12);
  var1p = (((((int64_t) 1) << 47) + var1p)) * ((int64_t) BMP280.compensate_dig_P1) >> 33;
  if(var1p == 0){
    return 0;
  }
  p = 1048576 - (*_ReadPressure);
  p = (((p << 31) - var2p) * 3125)/ var1p;
  var1p = (((int64_t) BMP280.compensate_dig_P9) * (p >> 13) * (p >> 13)) >> 25;
  var2p = (((int64_t) BMP280.compensate_dig_P8) * p) >> 19;
  p = ((p + var1p + var2p) >> 8) + (((int64_t) BMP280.compensate_dig_P7) << 4);
  return (uint32_t) p;
}



status_t BMP280_Measure(int32_t* Temperature, uint32_t* Pressure){
  uint8_t TxData;
  bool_t Error = 0;

  TxData = BMP280_ReadRegisterMacro(BMP280_press_msb_RegAddr);
  BMP280__StartTransmision();
    Error += HAL_SPI_Transmit(BMP280.hspi, &TxData, sizeof(uint8_t), 50);
    Error += HAL_SPI_Receive(BMP280.hspi, (uint8_t*) &BMP280.Reg_press_msb, sizeof(uint8_t), 50);
    Error += HAL_SPI_Receive(BMP280.hspi, (uint8_t*) &BMP280.Reg_press_lsb, sizeof(uint8_t), 50);
    Error += HAL_SPI_Receive(BMP280.hspi, (uint8_t*) &BMP280.Reg_press_xlsb, sizeof(uint8_t), 50);
    Error += HAL_SPI_Receive(BMP280.hspi, (uint8_t*) &BMP280.Reg_temp_msb, sizeof(uint8_t), 50);
    Error += HAL_SPI_Receive(BMP280.hspi, (uint8_t*) &BMP280.Reg_temp_lsb, sizeof(uint8_t), 50);
    Error += HAL_SPI_Receive(BMP280.hspi, (uint8_t*) &BMP280.Reg_temp_xlsb, sizeof(uint8_t), 50);
  BMP280__StopTransmision();

  int32_t ReadPressure;
  int32_t ReadTemperature;

  uint32_t* uRead = (uint32_t*) &ReadPressure;
  uint32_t _xlsb32 = BMP280.Reg_press_xlsb;
  uint32_t _lsb32 = BMP280.Reg_press_lsb;
  uint32_t _msb32 = BMP280.Reg_press_msb;
  *uRead = (_xlsb32 >> 4) | (_lsb32 << 4) | (_msb32 << 12);

  uRead = (uint32_t*) &ReadTemperature;
  _xlsb32 = BMP280.Reg_temp_xlsb;
  _lsb32 = BMP280.Reg_temp_lsb;
  _msb32 = BMP280.Reg_temp_msb;
  *uRead = (_xlsb32 >> 4) | (_lsb32 << 4) | (_msb32 << 12);

  int32_t pom;
  *Temperature = BMP280__CompensateTemperature(&ReadTemperature, &pom);
  *Pressure = BMP280__CompensatePressure(&ReadPressure, &pom);
  (*Pressure) /= 256;

  return (Error == 0) ? BMP280_SUCCES : BMP280_ERROR;
}
