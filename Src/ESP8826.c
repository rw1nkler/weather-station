/*
 * ESP8826.c
 *
 *  Created on: 06.04.2018
 *      Author: robert
 */

#include "ESP8826.h"
#include <string.h>
#include <stdlib.h>

/*!
 * \brief Inicjalizuje ESP8826
 *
 * Funkcja inicjalizuje struktur� przechowuj�c� informacje niezb�dne dla
 * poprawnego dzia�ania modu�u WiFi ESP8826. Przechowuje m.in. informacje
 * o tym, kt�ry UART jest wykorzystywany do komunikacji z modu�em.
 *
 * \param[in] ESP8826 - Struktura kt�ra b�dzie przechowywac informacje
 * dotycz�ce modu�u i jego konfiguracji.
 * \param[in] _huart - Handler do UART'a z kt�rego korzysta modu�.
 */
status_t ESP8826_Init(ESP8826Struct* ESP8826, UART_HandleTypeDef* _huart){
  ESP8826->huart = _huart;
  ESP8826->_Network = NULL;
  ESP8826->_Password = NULL;
  ESP8826->_ConnectionType = NULL;
  ESP8826->_ServerIP = NULL;
  ESP8826->_Port = NULL;
  ESP8826->FatalError = 0;

  ESP8826->_ServerSettingsSet = 0;
  ESP8826->_WiFiSettingsSet = 0;

  uint8_t TxData[] = "ATE0\r\n";            // Wy��czenie powtarzania komend w odpowiedzi ESP
  uint8_t RxData[14]; RxData[13] = '\0';    // Przewidywana najdluzsza odpowiedz + znak konca stringu
                                            // 13 65 84 69 48 13 13 10 13 10 79 75 13 <- ASCII

  HAL_UART_Transmit(ESP8826->huart, TxData, (uint16_t) strlen((const char*) TxData), 50);
  HAL_UART_Receive(ESP8826->huart, RxData, 13, 50);

  if(strstr((const char*) RxData, "OK") != NULL) return ESP8826_SUCCESS;
  else return ESP8826_ERROR;
}

/*!
 * \brief Sprawdza po��czenie z modu�em WiFi
 *
 * Sprawdza czy modu� zwr�ci standardow� odpowied� na zapytanie dotycz�ce
 * komunikacji z nim. Korzysta z komend AT.
 *
 * \param[in] ESP8826 - Struktura przechowuj�ca informacje dotycz�ce modu�u i jego konfiguracji.
 *
 * \retval true - Je�eli modu� nawi�za� po��czenie i potwierdzi� to odpowiedni� komend�.
 * \retval false - Je�eli modu� nie odpowiada na po��czenie.
 */
bool_t ESP8826_CheckConnection(ESP8826Struct* ESP8826){
  uint8_t TxData[] = "AT\r\n";               // Komenda sprawdzaj�ca po��czenie z ESP
  uint8_t RxData[7]; RxData[6] = '\0';       // Przewidywana odpowiedz + znak konca stringu
                                             // 13 10 79 75 13 10 <- ASCII
  HAL_UART_Transmit(ESP8826->huart, TxData, (uint16_t) strlen((const char*) TxData), 50);
  HAL_UART_Receive(ESP8826->huart, RxData, 6, 50);

  if(strstr((const char*) RxData, "OK") != NULL) return 1;
  else return 0;
}

/*!
 * \brief Pobiera wersj� firmware'u z modu�u
 *
 * Pobiera wersj� firmware'u z modu�u i zapisuje go w pami�ci alokuj�c j�.
 * Po odczytaniu tekstu nale�y zwolnic pami�c u�ywaj�c free().
 *
 * \param[in] ESP8826 - Struktura przechowuj�ca informacje dotycz�ce modu�u i jego konfiguracji.
 * \param[out] Firmware - Wska�nik na string z tekstem dotycz�cym wersji firmware'u. Pami�c na niego
 * zostaje zaalokowana funkcj� malloc().
 * \param[out] size - Wielko�c stringu firmware z uwzgl�dneniem znaku '\0'
 *
 * \retval ESP8826_SUCCES - Je�eli modu� zwr�ci string z wersj� firmware'u.
 * \retval ESP8826_ERROR - Je�eli nie uda sie nawi�zac poprawnego po��czenia z modu�em.
 */

/* TODO
status_t ESP8826_ReadFirmwareVersion(ESP8826Struct* ESP8826, char** Firmware, uint8_t* size){
  uint8_t TxData[] = "AT+GMR\r\n";  // Komenda AT
  uint8_t RxData[200];
  HAL_UART_Transmit(ESP8826->huart, TxData, 8, 50);
  HAL_UART_Receive(ESP8826->huart, RxData, 200, 50);

  uint8_t* Possition;
  Possition = (uint8_t*) strstr((const char*) RxData,"OK"); // Zwraca wska�nik do miejsca pojawienia si� po raz pierwszy "OK"
  uint8_t i = 0;                                            // tzn. Zaraz po wy�wietleniu firmware'u.
  uint8_t* tmp = &(RxData[9]);          // Domy�lnie ESP8826 n pocz�tku odpowiedzi na komend� zwraca komend� na kt�r� odpowiada
                                        // Pomijam to.
  if(Possition != NULL){                // Licz� jak d�ugi jest tekst odpowiedzi
    while(tmp != Possition){
      ++tmp;
      ++i;
    }
    i -= 2;                             // Cofam licznik o dwa ostatie znaki tj. "\r\n"

    (*size) = i+1;                      // D�ugo�c tekstu z uwzgl�dnieniem '\0'
    (*Firmware) = (char*) calloc(*size, sizeof(uint8_t));

    if((*Firmware) == NULL){            // Je�eli nie uda�o si� zaalokowac pami�ci zwracam ESP8826_ERROR
      *size = 0;
      return ESP8826_ERROR;
    }
    else{                               // Je�eli uda�o si� zaalokowac pami�c to wpisuje do niej string z wersj� firmware'u
      memcpy((void*) *Firmware, &(RxData[9]), i);
      (*Firmware)[i] = '\0';
      return ESP8826_SUCCESS;
    }
  }

  else return ESP8826_ERROR;            // Je�eli modu� nie odpowiedzia� zwracam ESP8826_ERROR
  }
*/

/*!
 * \brief Ustawia tryb pracy modu�u
 *
 * Ustawia tryb pracy modu�u na jeden z trzech dost�pnych:
 * - Klient
 * - Serwer
 * - Klient / Serwer
 *
 * Do obs�ugi wygodnie jest uzyc zdefiniowanych dyrektyw preprocesora:
 * - ESP8826_Mode_Client                ustawia tryp na Klienta
 * - ESP8826_Mode_Server                ustawia tryb na Serwer
 * - ESP8826_Mode_ClientServer          ustawia tryb na Klient/Serwer
 *
 * \param[in] ESP8826 - Struktura przechowuj�ca informacje dotycz�ce modu�u i jego konfiguracji.
 * \param[in] _Mode - Znak ASCII okre�laj�cy, kt�ry tryb pracy modu�u b�dzie wybrany.
 * Zaleca si� stosowanie zdefiniowanych dyrektyw preprocesora.
 *
 * \retval ESP8826_SUCCES - Je�eli modu� zwr�ci poprawn� odpowied� na zapytanie dotycz�ce zmiany trybu.
 * \retval ESP8826_ERROR - Je�eli nie uda si� nawi�zac poprawnego po��czenia z modu�em.
 */
status_t ESP8826_SetMode(ESP8826Struct* ESP8826, uint8_t _Mode){
  uint8_t TxData[] = "AT+CWMODE_CUR=X\r\n";  // Komenda AT ustawiaj�ca tryb
  TxData[14] = _Mode;                        // Podmienienie X na wybrany tryb

  uint8_t RxData[7]; RxData[6] = '\0';       // Przewidywana odpowiedz + znak konca stringu
                                             // 13 10 79 75 13 10 <- ASCII

  HAL_UART_Transmit(ESP8826->huart, TxData, strlen((const char*) TxData), 50);
  HAL_UART_Receive(ESP8826->huart, RxData, 6, 50);
  if(strstr((const char*) RxData, "OK") != NULL) return ESP8826_SUCCESS;
    else return ESP8826_ERROR;
}

/*!
 * \brief Ustawia parametry po��czenia z WiFi
 *
 * Ustawia parametry po��czenia z WiFi takie jak nazwa i has�o.
 * Niezb�dne do poprawnego dzia�ania funkcji ConnectWithWifi
 *
 * \param[in] ESP8826 - Struktura przechowuj�ca informacje dotycz�ce modu�u i jego konfiguracji.
 * \param[in] Network - Nazwa sieci WiFi do kt�rej chcemy si� po��czyc
 * \param[in] Password - Has�o do sieci z kt�r� chcemy si� po��czyc
 *
 */

void ESP8826_SetWiFiSettings(ESP8826Struct* ESP8826, const char* Network, const char* Password){
  if(ESP8826->_Network != NULL) free(ESP8826->_Network);
  if(ESP8826->_Password != NULL) free(ESP8826->_Password);

  ESP8826->_Network = (char*) calloc(strlen(Network) + 1, sizeof(char));
  ESP8826->_Password = (char*) calloc(strlen(Password) + 1, sizeof(char));

  strcpy(ESP8826->_Network, Network);
  strcpy(ESP8826->_Password, Password);

  ESP8826->_WiFiSettingsSet = 1;
}

/*!
 * \brief Nawi�zuje po��czenie z WiFi.
 *
 * Nawi�zuje po��czenie w sieci WiFi, wykorzystuj�c przekazan� nazw� i has�o.
 * Do poprawnego dzia�ania funkcji trzeba najpierw wywo�a� ESP8826_SetWiFiSettings(...)
 *
 * \param[in] ESP8826 - Struktura przechowuj�ca informacje dotycz�ce modu�u i jego konfiguracji.
 *
 * \retval ESP8826_SUCCESS - Je�eli modu� zwr�ci poprawn� odpowied� na zapytaie dotycz�ce po��czenia z sieci� WiFi.
 * \retval ESP8826_ERROR - Je�eli nie uda si� nawi�zac poprawnego po��czenia z modu�em.
 */
status_t ESP8826_ConnectWithWiFi(ESP8826Struct* ESP8826){
  if(ESP8826->_WiFiSettingsSet == 0) return ESP8826_ERROR;

  uint8_t length = strlen(ESP8826->_Network) + strlen(ESP8826->_Password) + 21;    // Liczenie ile znak�w wys�ac przez UART + znak konca stringu
                                                                 // AT+CWJAP_CUR="siec","haslo"\r\n -> dodatkowo AT+CWJAP_CUR="",""\r\n == 20 + (znak konca)
  uint8_t* TxData = (uint8_t*) calloc(length, sizeof(uint8_t));  // Alokowanie pamieci na wiadomosc do ESP
  memset(TxData, '\0', length);
  strcpy((char*) TxData, "AT+CWJAP_CUR=\"");
  strcpy((char*) TxData, strcat(strcat(strcat(strcat((char*)TxData, ESP8826->_Network),"\",\""), ESP8826->_Password),"\"\r\n"));  // Komenda AT

  uint8_t* RxData = (uint8_t*) calloc(201, sizeof(uint8_t));
  memset(RxData,'X',53); RxData[52] = '\0';                      // Przewidywana najdluzsza odpowiedz
	  HAL_UART_Transmit(ESP8826->huart, TxData, strlen((const char*) TxData), 50);

  HAL_Delay(10000);
  uint8_t status = ESP8826_CheckConnectionStatus(ESP8826);

  if(status == ESP8826_STATUS_CONNECTED_TO_AP){
    free(TxData);
    free(RxData);
    return ESP8826_SUCCESS;
  }
  else{
    free(TxData);
    free(RxData);
  }
  return ESP8826_ERROR;
}

/*!
 * \brief Aktywuje deep sleep na okreslony czas.
 *
 * Aktywuje tryb g��bokiego pienia pobieraj�cy najmniejsz� ilo�c pr�du. Aby modu� WiFi m�g� si� obudzic
 * nale�y podpi�c pin RESET z pinem WAKEUP.
 *
 * \param[in] ESP8826 - Struktura przechowuj�ca informacje dotycz�ce modu�u i jego konfiguracji.
 * \param[in] time_ms - Czas u�pienia w milisekundach
 *
 * \retval ESP8826_SUCCESS - Je�eli modu� zwr�ci poprawn� odpowied� na zapytaie dotycz�ce po��czenia z sieci� WiFi.
 * \retval ESP8826_ERROR - Je�eli nie uda si� nawi�zac poprawnego po��czenia z modu�em.
 */
status_t ESP8826_DeepSleepModeEnable(ESP8826Struct* ESP8826, uint16_t time_ms){
  char buffer[33];
  itoa(time_ms, buffer, 10);
  uint8_t length = strlen(buffer) + 11;     // D�ugo�c komendy + znak konca stringu
                                            // AT+GSLP=\r\n + strlen(buffer) + znak konca
  uint8_t* TxData = (uint8_t*) calloc(length, sizeof(uint8_t));
  memset(TxData, '\0', length);
  strcat(strcat(strcat((char*)TxData, "AT+GSLP="), buffer), "\r\n");  // Sklejanie komendy
  uint8_t RxData[7]; RxData[6] = '\0';      // Przewidywana odpowiedz
                                           // 13 10 79 75 13 10 <- ASCII

  HAL_UART_Transmit(ESP8826->huart, TxData, strlen((const char*) TxData), 50);
  HAL_UART_Receive(ESP8826->huart, RxData, 6, 50);

  free(TxData);

  if(strstr((const char*) RxData, "OK") != NULL) return ESP8826_SUCCESS;
  else return ESP8826_ERROR;
}

/*!
 * \brief Ustawia parametry po��czenia z serwerem.
 *
 * Ustawia parametry po��czenia z serwerem takie jak typ polaczenia, ip i port.
 * Niezb�dne do poprawnego dzia�ania funkcji ConnectWithServer.
 *
 * \param[in] ESP8826 - Struktura przechowuj�ca informacje dotycz�ce modu�u i jego konfiguracji.
 * \param[in] type - Typ polaczenia z serwerem.
 * \param[in] ip - IP serwera.
 * \param[in] port - Port po kt�rym si� ��czymy.
 *
 */

void ESP8826_SetServerSettings(ESP8826Struct* ESP8826, const char* type, const char* ip, const char* port){
  if(ESP8826->_ConnectionType != NULL) free(ESP8826->_ConnectionType);
  if(ESP8826->_ServerIP != NULL) free(ESP8826->_ServerIP);
  if(ESP8826->_Port != NULL) free(ESP8826->_Port);

  ESP8826->_ConnectionType = (char*) calloc(strlen(type) + 1, sizeof(char));
  ESP8826->_ServerIP = (char*) calloc(strlen(ip) + 1, sizeof(char));
  ESP8826->_Port = (char*) calloc(strlen(port) + 1, sizeof(char));

  strcpy(ESP8826->_ConnectionType, type);
  strcpy(ESP8826->_ServerIP, ip);
  strcpy(ESP8826->_Port, port);

  ESP8826->_ServerSettingsSet = 1;
}

/*!
 * \brief Ustanawia po�aczenie z serwerem.
 *
 * Do ustanowienia po��aczenia niezb�dne jest wczesniejsze wywolanie funkcji SetServerSerrings(...)
 *
 * \param[in] ESP8826 - Struktura przechowuj�ca informacje dotycz�ce modu�u i jego konfiguracji.
 *
 * \retval ESP8826_SUCCES - Je�eli modu� zwr�ci poprawn� odpowied� na zapytanie dotycz�ce zmiany trybu.
 * \retval ESP8826_ERROR - Je�eli nie uda si� nawi�zac poprawnego po��czenia z modu�em.
 */
status_t ESP8826__ConnectWithServer(ESP8826Struct* ESP8826){
  if(ESP8826->_ServerSettingsSet == 0) return ESP8826_ERROR;

  uint8_t length = 21 + strlen(ESP8826->_ConnectionType) + strlen(ESP8826->_ServerIP) + strlen(ESP8826->_Port);
  uint8_t* TxData = (uint8_t*) calloc(length, sizeof(uint8_t));
  memset(TxData, '\0', length);
  strcat(strcat(strcat(strcat(strcat(strcat(strcat((char*) TxData, "AT+CIPSTART=\""), ESP8826->_ConnectionType), "\",\""), ESP8826->_ServerIP), "\","), ESP8826->_Port), "\r\n");
  uint8_t RxData[16]; RxData[15] = '\0';

  HAL_UART_Transmit(ESP8826->huart, TxData, strlen((const char*) TxData), 50);
  HAL_UART_Receive(ESP8826->huart, RxData, 15, 50);

  free(TxData);

  if(strstr((const char*) RxData, "OK") != NULL) return ESP8826_SUCCESS;
  else return ESP8826_ERROR;
}
/*!
 * \brief Wysy�a zapytanie GET do serwera.
 *
 * Wysy�a zapytanie korzystaj�c z wcze�niej ustawionych parametr�w po��czenia z serwerem.
 * Do poprawnego dzia�anie niezb�dne jest wcze�niejsze wykorzystanie funkcji SetServerSettings(...).
 *
 * \param[in] ESP8826 - Struktura przechowuj�ca informacje dotycz�ce modu�u i jego konfiguracji.
 * \param[in] message - Wiadomo�c wysy�ana do serwera.
 *
 * \retval ESP8826_SUCCES - Je�eli modu� zwr�ci poprawn� odpowied� na zapytanie dotycz�ce zmiany trybu.
 * \retval ESP8826_ERROR - Je�eli nie uda si� nawi�zac poprawnego po��czenia z modu�em.
 */
status_t ESP8826_SendGet(ESP8826Struct* ESP8826, const char* message){
  if(ESP8826__ConnectWithServer(ESP8826) == ESP8826_ERROR) return ESP8826_ERROR;

  uint8_t message_length = strlen(message) + 8;
  uint8_t message_digits;
  char buffer[4];

  if(message_length < 10) message_digits = 1;
  else if(message_length >= 10 && message_length < 100) message_digits = 2;
  else if(message_length >= 100 && message_length < 1000) message_digits = 3;
  else return ESP8826_ERROR;

  uint8_t length = 13 + message_digits;  //AT+CIPSEND=
  uint8_t* TxData = (uint8_t*) calloc(length, sizeof(uint8_t));
  memset(TxData, '\0', length);
  strcat(strcat(strcat((char*) TxData, "AT+CIPSEND="), itoa(message_length, buffer, 10)), "\r\n");
  uint8_t RxData[7]; RxData[6] = '\0';

  HAL_UART_Transmit(ESP8826->huart, TxData, strlen((const char*) TxData), 50);
  HAL_UART_Receive(ESP8826->huart, RxData, 6, 50);

  free(TxData);
  if(strstr((const char*) RxData, "OK") == NULL) {
	  return ESP8826_ERROR;
  }
  else{
	  TxData = (uint8_t*) calloc(message_length, sizeof(uint8_t));
	  uint8_t* RxData2 = (uint8_t*) calloc(28+message_digits, sizeof(uint8_t)); RxData2[27+message_digits] = '\0';
	  strcat(strcat(strcat((char*) TxData, "GET "), message), "\r\n\r\n");

	  HAL_UART_Transmit(ESP8826->huart, TxData, strlen((const char*) TxData), 50);
	  HAL_UART_Receive(ESP8826->huart, RxData2, 27+message_digits, 50);

	  free(TxData);
	  if(strstr((const char*) RxData2, "SEND OK") != NULL){
		  free(RxData2);
		  return ESP8826_SUCCESS;
	  }
	  else {
		  free(RxData2);
		  return ESP8826_ERROR;
	  }
  }

  return ESP8826_ERROR;
}

/*!
 * \brief Sprawdza status po��czenia ESP.
 *
 * Pokazuje w jakim stanie jest po��czenie ESP. Do obs�ugi zaleca si� wykorzystanie przygotowanych
 * definicji preprocesora:
 *  - ESP8826_STATUS_CONNECTED_TO_AP [2]
 *  - ESP8826_STATUS_CREATED_TCP_OR_UDP_TRANSMISSION [3]
 *  - ESP8826_STATUS_DISCONNESTED_FROM_TCP_UDP [4]
 *  - ESP8826_STATUS_DISCONNECTED_FROM_AP [5]
 *
 *  \param[in] ESP8826 - Struktura przechowuj�ca informacje dotycz�ce modu�u i jego konfiguracji.
 *
 *  \retval ESP8826_STATUS_CONNECTED_TO_AP                     - Je�eli modu� jest po��czony z Routerem
 *  \retval ESP8826_STATUS_CREATED_TCP_OR_UDP_TRANSMISSION     - Je�eli nawi�zane jest po��czenie TCP albu UDP
 *  \retval ESP8826_STATUS_DISCONNESTED_FROM_TCP_UDP           - Je�eli modu� wy���czy� po��czenie TCP albo UDP
 *  \retval ESP8826_STATUS_DISCONNECTED_FROM_AP                - Je�eli modu� nie jest po��czony z AP
 */

uint8_t ESP8826_CheckConnectionStatus(ESP8826Struct* ESP8826){
  uint8_t TxData[] = "AT+CIPSTATUS\r\n";
  uint8_t RxData[10]; RxData[9] = '\0';

  HAL_UART_Transmit(ESP8826->huart, TxData, strlen((const char*) TxData), 50);
  HAL_UART_Receive(ESP8826->huart, RxData, 14, 50);

  return RxData[8];
}
