/*
 * 1-Wire.c
 *
 *  Created on: 01.05.2018
 *      Author: robert
 */

#include "1-Wire.h"
#include <string.h>

volatile uint8_t licznik = 0;


void OneWire_Init(OneWireStruct* OneWireInterface, TIM_HandleTypeDef* htim, uint32_t timOCChannel, GPIO_TypeDef* GPIOx, uint16_t GPIO_Pin){
  OneWireInterface->htim_ptr = htim;
  OneWireInterface->timOCChannel = timOCChannel;
  OneWireInterface->GPIOx = GPIOx;
  OneWireInterface->GPIO_Pin = GPIO_Pin;

  OneWireInterface->InitProcFailure_Callback = NULL;
  OneWireInterface->WriteEnd_Callback        = NULL;

  OneWireInterface->TransmissionState = ONEWIRE_TRANSMISSION_STATE_NONE;
  OneWireInterface->TransmissionSubState = ONEWIRE_TRANSMISSION_SUBSTATE_NONE;

  memset((void*) OneWireInterface->CheckPresenceTab, 2, 40);
  OneWireInterface->PresenceIterator = 0;

  OneWire__SendBufferInit(OneWireInterface);

  memset((void*) OneWireInterface->CurrentOperation, 0, 10);
  OneWireInterface->CurrentOperationIterator = 0;
  OneWireInterface->CurrentOperationAmount = 0;

  OneWireInterface->ReadBuffer = NULL;
  OneWireInterface->ReadBufferSize = 0;
  OneWireInterface->ReadBufferIterator = 0;

  memset((void*) OneWireInterface->Buffer, 0, 2);

  HAL_GPIO_WritePin(OneWireInterface->GPIOx, OneWireInterface->GPIO_Pin, GPIO_PIN_SET);
}

void OneWire__SendBufferInit(OneWireStruct* OneWireInterface){
	OneWireInterface->SendBuffer = 0;
	OneWireInterface->SendBufferIterator = 1;
	OneWireInterface->SendBufferTabIterator = 0;
	OneWireInterface->SendBufferTabSize = 0;
}

void OneWire__SetInterval_IT(OneWireStruct* OneWireInterface, uint32_t time_us){
  uint32_t TimerCNTNext = __HAL_TIM_GET_COUNTER(OneWireInterface->htim_ptr) + time_us*10;
  __HAL_TIM_SET_COMPARE(OneWireInterface->htim_ptr, OneWireInterface->timOCChannel, TimerCNTNext);
  HAL_TIM_OC_Start_IT(OneWireInterface->htim_ptr, OneWireInterface->timOCChannel);
}

void OneWire_SetInitProcFailure_Callback(OneWireStruct* OneWireInterface, void (*funPointer)()){
	OneWireInterface->InitProcFailure_Callback = funPointer;
}

void OneWire_SetWriteEnd_Callback(OneWireStruct* OneWireInterface, void (*funPointer)()){
	OneWireInterface->WriteEnd_Callback = funPointer;
}

void OneWire_SetReadEnd_Callback(OneWireStruct* OneWireInterface, void (*funPointer)()){
	OneWireInterface->ReadEnd_Callback = funPointer;
}

void OneWire_SetEndOfTransmission_Callback(OneWireStruct* OneWireInterface, void (*funPointer)()){
	OneWireInterface->EndOfTransmission_Callback = funPointer;
}

void OneWire__InitializationProcedure(OneWireStruct* OneWireInterface){
  OneWireInterface->TransmissionState = ONEWIRE_TRANSMISSION_STATE_INITIALIZATION;
  OneWireInterface->TransmissionSubState = ONEWIRE_TRANSMISSION_INITIALIZATION_SUBSTATE_PULLUPLINE;
  OneWire__ConfigurePinAsOutput_LOW(OneWireInterface);
  //HAL_GPIO_TogglePin(PinTest_GPIO_Port, PinTest_Pin);
  OneWire__SetInterval_IT(OneWireInterface, 520);
}

void OneWire__ConfigurePinAsInput(OneWireStruct* OneWireInterface){
  GPIO_InitTypeDef GPIO_InitStruct;
  GPIO_InitStruct.Pin = OneWireInterface->GPIO_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;

  HAL_GPIO_Init(OneWireBus_GPIO_Port, &GPIO_InitStruct);
}

void OneWire__ConfigurePinAsOutput_HIGH(OneWireStruct* OneWireInterface){
  GPIO_InitTypeDef GPIO_InitStruct;
  GPIO_InitStruct.Pin = OneWireInterface->GPIO_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_INPUT;
  GPIO_InitStruct.Pull = GPIO_NOPULL;

  HAL_GPIO_Init(OneWireBus_GPIO_Port, &GPIO_InitStruct);
}

void OneWire__ConfigurePinAsOutput_LOW(OneWireStruct* OneWireInterface){
  GPIO_InitTypeDef GPIO_InitStruct;
  GPIO_InitStruct.Pin = OneWireInterface->GPIO_Pin;
  GPIO_InitStruct.Mode = GPIO_MODE_OUTPUT_OD;
  GPIO_InitStruct.Pull = GPIO_NOPULL;
  GPIO_InitStruct.Speed = GPIO_SPEED_FREQ_VERY_HIGH;

  HAL_GPIO_Init(OneWireBus_GPIO_Port, &GPIO_InitStruct);
  HAL_GPIO_WritePin(OneWireInterface->GPIOx, OneWireInterface->GPIO_Pin, GPIO_PIN_RESET);
}

bool_t OneWire__CheckPresence(OneWireStruct* OneWireInterface){
	uint8_t counter = 0;
	for(uint8_t i = 0; i < 40; ++i){
		if(OneWireInterface->CheckPresenceTab[i] == 0) ++counter;
	}
	return (counter >= 6 && counter <= 24)? 1 : 0;
}

void OneWire_WriteCommand_SkipROM_IT(OneWireStruct* OneWireInterface, volatile  uint8_t *TwoElementTabBuffer, uint8_t Command){
	OneWire__AddOperation(OneWireInterface, ONEWIRE_TRANSMISSION_STATE_SEND);
	OneWire__AddOperation(OneWireInterface, ONEWIRE_TRANSMISSION_STATE_END);
	OneWireInterface->SendBuffer = TwoElementTabBuffer;
	OneWireInterface->SendBuffer[0] = 0xCC;
	OneWireInterface->SendBuffer[1] = Command;
	OneWireInterface->SendBufferIterator = 1;
	OneWireInterface->SendBufferTabIterator = 0;
	OneWireInterface->SendBufferTabSize = 2;

	OneWire__InitializationProcedure(OneWireInterface);
}

void OneWire_Read_SkipROM_IT(OneWireStruct* OneWireInterface, volatile  uint64_t* ReadBuffer, volatile  uint8_t ReadBufferSize){
    OneWireInterface->ReadBuffer = ReadBuffer;
    OneWireInterface->ReadBufferSize = ReadBufferSize;
    OneWireInterface->ReadBufferIterator = 0;
    OneWire__CleanReadBuffer(OneWireInterface);
	OneWireInterface->SendBuffer = OneWireInterface->Buffer;
	OneWireInterface->SendBuffer[0] = 0xCC;
	OneWireInterface->SendBuffer[1] = 0xBE;
	OneWireInterface->SendBufferIterator = 1;
	OneWireInterface->SendBufferTabIterator = 0;
	OneWireInterface->SendBufferTabSize = 2;
	OneWire__AddOperation(OneWireInterface, ONEWIRE_TRANSMISSION_STATE_SEND);
	OneWire__AddOperation(OneWireInterface, ONEWIRE_TRANSMISSION_STATE_READ);
	OneWire__AddOperation(OneWireInterface, ONEWIRE_TRANSMISSION_STATE_END);

	OneWire__InitializationProcedure(OneWireInterface);
}

void OneWire__PutReadBuffer(OneWireStruct* OneWireInterface, uint8_t Bit){
	if(OneWireInterface->ReadBufferIterator < OneWireInterface->ReadBufferSize){
		(*OneWireInterface->ReadBuffer) |= (Bit << OneWireInterface->ReadBufferIterator);
		++OneWireInterface->ReadBufferIterator;
		++licznik;
	}
}

uint32_t DS18B20_ReadTemp(uint16_t Measured){
	float result = (float)(Measured >> 4);
	uint16_t tmp = (Measured & 0x000F);
	result += (float)(tmp / 16.0);
	return result*100;
}

void OneWire__CleanReadBuffer(OneWireStruct* OneWireInterface){
	(*OneWireInterface->ReadBuffer) = 0;
}

void OneWire__CleanPresenceTab(OneWireStruct* OneWireInterface){
	memset((void*) OneWireInterface->CheckPresenceTab, 2, 40);
	OneWireInterface->PresenceIterator = 0;
}

inline void OneWire__Master_Write_SLOTBEGIN(OneWireStruct* OneWireInterface){
  OneWire__ConfigurePinAsOutput_LOW(OneWireInterface);
  OneWire__SetInterval_IT(OneWireInterface, 10);
}

int16_t OneWire__FetchBuffer(OneWireStruct* OneWireInterface){
	if(OneWireInterface->SendBufferTabIterator < OneWireInterface->SendBufferTabSize){
		if(OneWireInterface->SendBufferIterator <= 128){
			if(OneWireInterface->SendBuffer[OneWireInterface->SendBufferTabIterator] & OneWireInterface->SendBufferIterator){
				OneWireInterface->SendBufferIterator <<= 1;
				return 1;
			}
			else{
				OneWireInterface->SendBufferIterator <<= 1;
				return 0;
			}
		}
		else{
			++OneWireInterface->SendBufferTabIterator;
			OneWireInterface->SendBufferIterator = 1;
			return 2;
		}
	}
	else{
		OneWireInterface->SendBufferIterator = 1;
		OneWireInterface->SendBufferTabIterator = 0;
		return -1;
	}
}

uint8_t OneWire__FetchOperation(OneWireStruct* OneWireInterface){
	if(OneWireInterface->CurrentOperationAmount == 0) return 0;
	else{
		uint8_t result = OneWireInterface->CurrentOperation[OneWireInterface->CurrentOperationIterator];
		--OneWireInterface->CurrentOperationAmount;
		OneWireInterface->CurrentOperationIterator = (OneWireInterface->CurrentOperationIterator + 1) % 10;
		return result;
	}
}

uint8_t OneWire__AddOperation(OneWireStruct* OneWireInterface, operation_t Operation){
    if(OneWireInterface->CurrentOperationAmount == 10) return 255;
    else{
    	uint8_t index = (OneWireInterface->CurrentOperationIterator + OneWireInterface->CurrentOperationAmount) % 10;
    	OneWireInterface->CurrentOperation[index] = Operation;
    	++OneWireInterface->CurrentOperationAmount;
    	return index;
    }
}

void OneWire__DisplayOperationCircularBuffer(OneWireStruct* OneWireInterface){
	for(uint8_t i = 0; i < 10; ++i){
		printf("%d ", OneWireInterface->CurrentOperation[i]);
	}
	printf("\r\n");
}

void OneWire_OC_DelayElapsedCallback_USE_IT_IN_CALLBACK(OneWireStruct* OneWireInterface){
  if(OneWireInterface->TransmissionState == ONEWIRE_TRANSMISSION_STATE_INITIALIZATION){
	  if(OneWireInterface->TransmissionSubState == ONEWIRE_TRANSMISSION_INITIALIZATION_SUBSTATE_PULLUPLINE){
		  //HAL_GPIO_TogglePin(PinTest_GPIO_Port, PinTest_Pin);
		 OneWire__ConfigurePinAsInput(OneWireInterface);
		 //HAL_GPIO_TogglePin(PinTest_GPIO_Port, PinTest_Pin);
		 OneWire__SetInterval_IT(OneWireInterface, 5);

		 OneWireInterface->TransmissionSubState = ONEWIRE_TRANSMISSION_INITIALIZATION_SUBSTATE_WATCHPRESENCEPULSES;

	  }
	  else if(OneWireInterface->TransmissionSubState == ONEWIRE_TRANSMISSION_INITIALIZATION_SUBSTATE_WATCHPRESENCEPULSES){
		OneWireInterface->CheckPresenceTab[OneWireInterface->PresenceIterator] = HAL_GPIO_ReadPin(OneWireInterface->GPIOx, OneWireInterface->GPIO_Pin);
		OneWire__SetInterval_IT(OneWireInterface, 10);
		++OneWireInterface->PresenceIterator;
	    if(OneWireInterface->PresenceIterator >= 40){
	    	OneWire__SetInterval_IT(OneWireInterface, 10);
	    	OneWireInterface->TransmissionSubState = ONEWIRE_TRANSMISSION_INITIALIZATION_SUBSTATE_CHECKPRESENCE;
	    }
	  }
	  else if(OneWireInterface->TransmissionSubState == ONEWIRE_TRANSMISSION_INITIALIZATION_SUBSTATE_CHECKPRESENCE){
        if(OneWire__CheckPresence(OneWireInterface)){
        	OneWireInterface->TransmissionState = ONEWIRE_TRANSMISSION_STATE_NEXT;
        	OneWireInterface->TransmissionSubState = ONEWIRE_TRANSMISSION_NEXT_SUBSTATE_NONE;
    		//HAL_GPIO_TogglePin(PinTest_GPIO_Port, PinTest_Pin);
        }
        else{
        	OneWireInterface->TransmissionState = ONEWIRE_TRANSMISSION_STATE_NONE;
        	OneWireInterface->TransmissionSubState = ONEWIRE_TRANSMISSION_SUBSTATE_NONE;
        	HAL_TIM_OC_Stop_IT(OneWireInterface->htim_ptr, OneWireInterface->timOCChannel);
        	if(OneWireInterface->InitProcFailure_Callback != NULL) OneWireInterface->InitProcFailure_Callback();
        }
        OneWire__SetInterval_IT(OneWireInterface, 1);
	  }
  }
  else if(OneWireInterface->TransmissionState == ONEWIRE_TRANSMISSION_STATE_NEXT){
	  uint8_t operation = OneWire__FetchOperation(OneWireInterface);
	  OneWireInterface->TransmissionState = operation;
		  OneWireInterface->TransmissionSubState = operation*10 + 1;
		  OneWire__SetInterval_IT(OneWireInterface, 1);
  }
  else if(OneWireInterface->TransmissionState == ONEWIRE_TRANSMISSION_STATE_SEND){
	  if(OneWireInterface->TransmissionSubState == ONEWIRE_TRANSMISSION_SEND_SUBSTATE_READBUFFER){
		  switch(OneWire__FetchBuffer(OneWireInterface)){
		  	  case 1:
		  		OneWire__Master_Write_SLOTBEGIN(OneWireInterface);
		  		OneWireInterface->TransmissionSubState = ONEWIRE_TRANSMISSION_SEND_SUBSTATE_WRITE1SLOT;
				 //HAL_GPIO_TogglePin(PinTest_GPIO_Port, PinTest_Pin);
		  		break;
		  	  case 0:
		  		OneWire__Master_Write_SLOTBEGIN(OneWireInterface);
		  		OneWireInterface->TransmissionSubState = ONEWIRE_TRANSMISSION_SEND_SUBSTATE_WRITE0SLOT;
				 //HAL_GPIO_TogglePin(PinTest_GPIO_Port, PinTest_Pin);
		  		break;
		  	  case -1:
		  		if(OneWireInterface->WriteEnd_Callback != NULL) OneWireInterface->WriteEnd_Callback();
		  		OneWireInterface->TransmissionState = ONEWIRE_TRANSMISSION_STATE_NEXT;
		  		OneWireInterface->TransmissionSubState = ONEWIRE_TRANSMISSION_NEXT_SUBSTATE_NONE;
				 //HAL_GPIO_TogglePin(PinTest_GPIO_Port, PinTest_Pin);
		  		OneWire__SetInterval_IT(OneWireInterface, 1);
		  		break;
		  	  case 2:
		  		OneWire__SetInterval_IT(OneWireInterface, 20);
		  		OneWireInterface->TransmissionSubState = ONEWIRE_TRANSMISSION_SEND_SUBSTATE_READBUFFER;
				 //HAL_GPIO_TogglePin(PinTest_GPIO_Port, PinTest_Pin);
		  		break;
		  }
	  }
	  else if(OneWireInterface->TransmissionSubState == ONEWIRE_TRANSMISSION_SEND_SUBSTATE_WRITE1SLOT){
		  OneWire__ConfigurePinAsOutput_HIGH(OneWireInterface);
		  OneWire__SetInterval_IT(OneWireInterface, 50);
		  OneWireInterface->TransmissionSubState = ONEWIRE_TRANSMISSION_SEND_SUBSTATE_PULLUPLINE;
	  }
	  else if(OneWireInterface->TransmissionSubState == ONEWIRE_TRANSMISSION_SEND_SUBSTATE_WRITE0SLOT){
		  OneWire__ConfigurePinAsOutput_LOW(OneWireInterface);
		  OneWire__SetInterval_IT(OneWireInterface, 49);
		  OneWireInterface->TransmissionSubState = ONEWIRE_TRANSMISSION_SEND_SUBSTATE_PULLUPLINE;
	  	  }
	  else if(OneWireInterface->TransmissionSubState == ONEWIRE_TRANSMISSION_SEND_SUBSTATE_PULLUPLINE){
		  OneWire__SetInterval_IT(OneWireInterface, 1);
		  OneWire__ConfigurePinAsOutput_HIGH(OneWireInterface);
		  OneWireInterface->TransmissionSubState = ONEWIRE_TRANSMISSION_SEND_SUBSTATE_READBUFFER;
	  }

  }
  else if(OneWireInterface->TransmissionState == ONEWIRE_TRANSMISSION_STATE_READ){
	  if(OneWireInterface->TransmissionSubState == ONEWIRE_TRANSMISSION_READ_SUBSTATE_PULLDOWNLINE){
		  if(OneWireInterface->ReadBufferIterator < OneWireInterface->ReadBufferSize){
			  OneWire__ConfigurePinAsOutput_LOW(OneWireInterface);
			  OneWireInterface->TransmissionSubState = ONEWIRE_TRANSMISSION_READ_SUBSTATE_PULLUPLINE;
			  OneWire__SetInterval_IT(OneWireInterface, 5);
		  }
		  else{
			  if(OneWireInterface->ReadEnd_Callback != NULL) OneWireInterface->ReadEnd_Callback();
			  OneWireInterface->TransmissionState = ONEWIRE_TRANSMISSION_STATE_NEXT;
			  OneWireInterface->TransmissionSubState = ONEWIRE_TRANSMISSION_NEXT_SUBSTATE_NONE;
			  //HAL_GPIO_TogglePin(PinTest_GPIO_Port, PinTest_Pin);
			  OneWire__SetInterval_IT(OneWireInterface, 1);
		  }
	  }
	  else if(OneWireInterface->TransmissionSubState == ONEWIRE_TRANSMISSION_READ_SUBSTATE_PULLUPLINE){
		  OneWire__ConfigurePinAsInput(OneWireInterface);
		  OneWireInterface->TransmissionSubState = ONEWIRE_TRANSMISSION_READ_SUBSTATE_CHECKSTATE;
		  OneWire__SetInterval_IT(OneWireInterface, 1);
	  }
	  else if(OneWireInterface->TransmissionSubState == ONEWIRE_TRANSMISSION_READ_SUBSTATE_CHECKSTATE){
		  OneWire__PutReadBuffer(OneWireInterface, HAL_GPIO_ReadPin(OneWireInterface->GPIOx, OneWireInterface->GPIO_Pin));
		  OneWire__SetInterval_IT(OneWireInterface, 49);
		  OneWireInterface->TransmissionSubState = ONEWIRE_TRANSMISSION_READ_SUBSTATE_PULLDOWNLINE;
	  }
  }
  else if(OneWireInterface->TransmissionState == ONEWIRE_TRANSMISSION_STATE_END){
	  if(OneWireInterface->TransmissionSubState == ONEWIRE_TRANSMISSION_END_SUBSTATE_NONE){
		  HAL_TIM_OC_Stop_IT(OneWireInterface->htim_ptr, OneWireInterface->timOCChannel);
		  OneWire__CleanPresenceTab(OneWireInterface);
		  if(OneWireInterface->EndOfTransmission_Callback != NULL) OneWireInterface->EndOfTransmission_Callback();
	  }
  }
}

